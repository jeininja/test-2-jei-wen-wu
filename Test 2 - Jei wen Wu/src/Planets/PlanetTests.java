package Planets;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

/**
 * @author Jei wen Wu
 * JUnit class to test all the methods/possibilities inside the Planet class
 */
class PlanetTests {

	/**
	 * Tests if all the get methods are returning the right field
	 */
	@Test
	void testGets() {
		Planet earth = new Planet("Solar", "Earth", 3, 6371);
		assertEquals("Solar", earth.getPlanetarySystemName());
		assertEquals("Earth", earth.getName());
		assertEquals(3, earth.getOrder());
		assertEquals(6371, earth.getRadius());
	}

	/**
	 * Tests if 2 Planet objects are equal everything is the same
	 */
	@Test
	void testEquals() {
		Planet planet1 = new Planet("Solar", "Earth", 3, 6371);
		Planet planet2 = new Planet("Solar", "Earth", 3, 6371);
		assertEquals(true, planet1.equals(planet2));
	}
	
	/**
	 * Tests if 2 Planets objects are equal if the names are different
	 */
	@Test
	void testEquals2() {
		Planet planet = new Planet("Solar", "Earth", 3, 6371);
		Planet planet2 = new Planet("Solar", "Mars", 3, 6371);
		assertEquals(false, planet.equals(planet2));
	}
	
	/**
	 * Tests if 2 Planet objects are equal if the planetarySystemNames are different
	 */
	@Test
	void testEquals3() {
		Planet planet = new Planet("Solar", "Earth", 3, 6371);
		Planet planet2 = new Planet("NotSolar", "Earth", 3, 6371);
		assertEquals(false, planet.equals(planet2));
	}
	
	/**
	 * Tests if 2 objects are equal if one of them isn't an instance of Planet
	 */
	@Test
	void testEquals4() {
		Planet planet = new Planet("Solar", "Earth", 3, 6371);
		int randomObject = 2;
		assertEquals(false, planet.equals(randomObject));
	}
	
	/**
	 * Tests if the hashCode returns the right int based on the name and PlanetarySysemName
	 */
	@Test
	void testHashCode() {
		Planet planet = new Planet("Solar", "Earth", 3, 6371);
		String combined = planet.getName()+planet.getPlanetarySystemName();
		int hashExpected = combined.hashCode();
		assertEquals(hashExpected, planet.hashCode());
	}
	
	/**
	 * Tests the compareTo method on names
	 */
	@Test
	void testCompareTo() {
		Planet planet1 = new Planet("Solar", "Earth", 3, 6371);
		Planet planet2 = new Planet("Solar", "Mars", 3, 6371);
		assertEquals(-1, planet1.compareTo(planet2));
	}
	
	/**
	 * Tests the compareTo method on names
	 */
	@Test
	void testCompareTo2() {
		Planet planet1 = new Planet("Solar", "Mars", 3, 6371);
		Planet planet2 = new Planet("Solar", "Earth", 3, 6371);
		assertEquals(1, planet1.compareTo(planet2));
	}
		
	/**
	 * Tests the compareTo method on radius
	 */
	@Test
	void testCompareTo3() {
		Planet planet1 = new Planet("Solar", "Earth", 3, 6370);
		Planet planet2 = new Planet("Solar", "Earth", 3, 6371);
		assertEquals(1, planet1.compareTo(planet2));
	}
	
	/**
	 * Tests the compareTo method on radius
	 */
	@Test
	void testCompareTo4() {
		Planet planet1 = new Planet("Solar", "Earth", 3, 6371);
		Planet planet2 = new Planet("Solar", "Earth", 3, 6370);
		assertEquals(-1, planet1.compareTo(planet2));
	}
	
	/**
	 * Tests the compareTo method on radius
	 */
	@Test
	void testCompareTo5() {
		Planet planet1 = new Planet("Solar", "Earth", 3, 6371);
		Planet planet2 = new Planet("Solar", "Earth", 3, 6371);
		assertEquals(0, planet1.compareTo(planet2));
	}
}