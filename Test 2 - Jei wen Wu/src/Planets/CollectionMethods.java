package Planets;

import java.util.*;

/**
 * @author Jei wen Wu
 * This class contains a method that returns a List of Planet objects with a radius larger or equal to the given size
 */
public class CollectionMethods {

	public static Collection<Planet> getLargerThan(Collection<Planet> planets, double size){
		
		List<Planet> bigPlanets = new ArrayList<Planet>();
		
		for(Planet p : planets) {
			if(p.getRadius()>=size) {
				bigPlanets.add(p);
			}
		}
		return bigPlanets;
	}
}
