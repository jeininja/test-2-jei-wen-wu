package Planets;

/**
 * A class to store information about a planet
 * @author Daniel
 *
 */
//implementing the Comparable interface so that 2 Planet objects are comparable using compareTo()
public class Planet implements Comparable<Planet>{
	/**
	 * The name of the solar system the planet is contained within
	 */
	private String planetarySystemName;
	
	/**
	 * The name of the planet.
	 */
	private String name;
	
	/**
	 * From inside to outside, what order is the planet. (e.g. Mercury = 1, Venus = 2, etc)
	 */
	private int order;
	
	/**
	 * The size of the radius in Kilometers.
	 */
	private double radius;

	/**
	 * A constructor that initializes the Planet object
	 * @param planetarySystemName The name of the system that the planet is a part of
	 * @param name The name of the planet
	 * @param order The order from inside to out of how close to the center the planet is
	 * @param radius The radius of the planet in kilometers
	 */
	public Planet(String planetarySystemName, String name, int order, double radius) {
		this.planetarySystemName = planetarySystemName;
		this.name = name;
		this.order = order;
		this.radius = radius;
	}
	
	/**
	 * @return The name of the planetary system (e.g. "the solar system")
	 */
	public String getPlanetarySystemName() {
		return planetarySystemName;
	}
	
	/**
	 * @return The name of the planet (e.g. Earth or Venus)
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return The rank of the planetary system
	 */
	public int getOrder() {
		return order;
	}


	/**
	 * @return The radius of the planet in question.
	 */
	public double getRadius() {
		return radius;
	}
	
	/**
	 * @author Jei wen Wu
	 * Override the equals method from the Object class so that two planet Objects are considered to be equal 
	 * if and only if both their names and planetarySystemName are identical
	 */
	@Override
	public boolean equals(Object o) {
		
		if(o instanceof Planet) {
			Planet p = (Planet)o;
			if(this.getPlanetarySystemName().equals(p.getPlanetarySystemName()) && this.getName().equals(p.getName())){
				return true;
			}
			else {
				return false;
			}
		}
		return false;
	}
	
	/**
	 * @author Jei wen Wu
	 * Override the hashCode method so that it is consistent with the equals method
	 */
	@Override
	public int hashCode() {
		String combined = this.getName()+this.getPlanetarySystemName();
		return combined.hashCode();
	}
	
	/**
	 * @author Jei wen Wu
	 * Adding the required method from the Comparable interface
	 * Compare the Planet objects first on their name(increasing order)
	 * and then on their radius (decreasing order)
	 */
	public int compareTo(Planet b) {
		int nameComparison = this.getName().compareTo(b.getName());
		if(nameComparison == 0)
		{
			if(this.getRadius()>b.getRadius())
				return -1;
			if(this.getRadius()<b.getRadius())
				return 1;
			else
				return 0;
		}
		else if(nameComparison>0) 
			return 1;
		else
			return -1;
	}
}

