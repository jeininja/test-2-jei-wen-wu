package Employees;
/**
 * @author Jei wen Wu
 *  A class to store information about a UnionizedHourlyEmployee
 */
public class UnionizedHourlyEmployee extends HourlyEmployee{

	private double maxHoursPerWeek;
	private double overtimeRate;
	
	public UnionizedHourlyEmployee(double hoursWorkedInAWeek, double hourlyPay, double maxHoursPerWeek, double overtimeRate) {
		super(hoursWorkedInAWeek, hourlyPay);
		this.maxHoursPerWeek = maxHoursPerWeek;
		this.overtimeRate = overtimeRate;
	}
	
	/**
	 * Override the getWeeklyPay method from the Employee interface to a more specific one
	 */
	@Override
	public double getWeeklyPay() {
		
		double weeklyPay = 0;
		double overtimeHours = 0;
		double overtimePay = 0;
		
		if(super.getHoursWorkedInAWeek()<=this.getMaxHoursPerWeek()) {
			weeklyPay = this.getHoursWorkedInAWeek() * this.getHourlyPay();
			return weeklyPay;
		}
		else {
			overtimeHours = super.getHoursWorkedInAWeek()-this.getMaxHoursPerWeek();
			overtimePay = overtimeHours * (this.getOvertimeRate() * this.getHourlyPay());
			weeklyPay = (this.getHoursWorkedInAWeek()-overtimeHours)*this.getHourlyPay();
			weeklyPay = weeklyPay + overtimePay;
		}
		return weeklyPay;
	}

	/**
	 * Getter method for the maxHoursPerWeek of a UnionizedHourlyEmployee
	 * @return maxHoursPerWeek an UnionizedHourlyEmployee is expected to work
	 */
	public double getMaxHoursPerWeek() {
		return maxHoursPerWeek;
	}

	/**
	 * Getter method for the overTimeRate of a UnionizedHourlyEmployee
	 * @return the rate the UnionizedHourlyEmployee gets paid when he works overtime
	 */
	public double getOvertimeRate() {
		return overtimeRate;
	}

}
