package Employees;
/**
 * @author Jei wen Wu
 *  A class to store information about a SalariedEmployee
 */
public class SalariedEmployee implements Employee{

	private double yearly;
	
	public SalariedEmployee(double yearly) {
		this.yearly = yearly;
	}
	
	/**
	 * Override the getWeeklyPay method from the Employee interface to a more specific one
	 */
	@Override
	public double getWeeklyPay() {
		return this.getYearly()/52;
	}

	/**
	 * Getter method for yearly salary
	 * @return the yearly salary of a SalariedEmployee
	 */
	public double getYearly() {
		return yearly;
	}

}
