package Employees;
/**
 * @author Jei wen Wu
 *  A class to store information about a Payroll
 */
public class PayrollManagement {

	private static double totalExpenses;
	
	public static double getTotalExpenses(Employee[] employees) {
		for(Employee e : employees) {
			totalExpenses = totalExpenses + e.getWeeklyPay();
		}
		return totalExpenses;
	}
	
	/**
	 * Main method where the code runs
	 * Fills an Employee[] with different types of employees and get the weekly totalExpenses of all the employees together
	 */
	public static void main(String[] args) {
		Employee[] employees = new Employee[5];
		employees[0]= new HourlyEmployee(20, 13.25);
		employees[1]= new HourlyEmployee(25, 12.25);
		employees[2]= new HourlyEmployee(30, 12);
		employees[3]= new SalariedEmployee(20000);
		employees[4]= new UnionizedHourlyEmployee(45, 10, 30, 1.5);
		
		double totalExpenses = getTotalExpenses(employees);
		System.out.println("Total expenses: "+totalExpenses+"$");
	}
	
}
