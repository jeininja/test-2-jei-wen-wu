package Employees;
/**
 * @author Jei wen Wu
 * A class to store information about a HourlyEmployee
 */
public class HourlyEmployee implements Employee{

	private double hoursWorkedInAWeek;
	private double hourlyPay;
	
	public HourlyEmployee(double hoursWorkedInAWeek, double hourlyPay) {
		this.hoursWorkedInAWeek = hoursWorkedInAWeek;
		this.hourlyPay = hourlyPay;
	}
	
	/**
	 * Override the getWeeklyPay method from the Employee interface to a more specific one
	 */
	@Override
	public double getWeeklyPay() {
		double weeklyPay = this.getHoursWorkedInAWeek() * this.getHourlyPay();
		return weeklyPay;
	}

	/**
	 * Getter method for the HoursWorkedInAWeek of an HourlyEmployee
	 * @return
	 */
	public double getHoursWorkedInAWeek() {
		return hoursWorkedInAWeek;
	}

	/**
	 * Getter method for the HourlyPay of an HourlyEmployee
	 * @return
	 */
	public double getHourlyPay() {
		return hourlyPay;
	}

}
