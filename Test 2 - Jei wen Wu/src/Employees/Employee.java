package Employees;
/**
 * @author Jei wen Wu
 * An interface Employee with the required getWeeklyPay to be implemented in its subclasses
 */
public interface Employee {

	public double getWeeklyPay();
	
}
